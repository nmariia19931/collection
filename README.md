# collection
Создайте класс с 2 полями, который будет использоваться при работе с коллекциями (например, класс User с полями age, name).
1.  Реализовать метод, который принимает на вход 2 листа и возвращает лист, состоящий только из тех элементов, которые присутствуют в обоих листах.
2.  Задание из 1го пункта, но вместо List нужно использовать Set.
3.  Создайте ArrayList с несколькими элементами (используя дубликаты) и проитерируйтесь по нему, выводя на консоль информацию в виде: <индекс>: <элемент>
4.  Создайте HashSet с несколькими элементами (используя дубликаты) и проитерируйтесь по нему, выводя на консоль информацию в виде: <элемент>
5.  Создайте HashMap с несколькими элементами (используя дубликаты) и проитерируйтесь по нему, выводя на консоль информацию в виде:
    a.  итерация по значениям: <значение>
    b.  итерация по ключам: <ключ>: <значение>
    c.  итерация по парам: <ключ>: <значение>


