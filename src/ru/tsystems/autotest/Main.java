package ru.tsystems.autotest;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        final User user1 = new User(10, "Masha");
        final User user2 = new User(11, "Lev");
        final User user3 = new User(11, "Sasha");
        final User user4 = new User(15, "Artur");
        final User user5 = new User(12, "Masha");
        final User user13 = new User(11, "Sasha");
        final User user14 = new User(15, "Artur");
        final User user100 = new User(13, "Lena");

        System.out.println("========ArrayList==================");
        ArrayList<User> usersList = new ArrayList<>();
        usersList.add(user1);
        usersList.add(user2);
        usersList.add(user3);
        usersList.add(user4);
        usersList.add(user5);
        usersList.add(user3);
        printListOfUsers(usersList);

        System.out.println("========HashSet==================");
        HashSet<User> usersSet = new HashSet<>();
        usersSet.add(user1);
        usersSet.add(user2);
        usersSet.add(user3);
        usersSet.add(user4);
        usersSet.add(user5);
        usersSet.add(user13);
        printSetOfUsers(usersSet);

        System.out.println("========HashMap==================");
        HashMap<String, Integer> usersHashMap = new HashMap<>();
        usersHashMap.put(user1.getName(), user1.getAge());
        usersHashMap.put(user2.getName(), user2.getAge());
        usersHashMap.put(user3.getName(), user3.getAge());
        usersHashMap.put(user4.getName(), user4.getAge());
        usersHashMap.put(user5.getName(), user5.getAge());
        for (Integer age : usersHashMap.values()) {
            System.out.println("value: " + age);
        }
        for (String name : usersHashMap.keySet()) {
            System.out.println("key: " + name);
        }
        for (Map.Entry user : usersHashMap.entrySet()) {
            System.out.println("key: " + user.getKey() + " with value " + user.getValue());
        }

        System.out.println("=========Find equals from lists==================");
        ArrayList<User> usersList1 = new ArrayList<>();
        usersList1.add(user1);
        usersList1.add(user3);
        usersList1.add(user4);
        usersList1.add(user5);
        System.out.println("=========List 1=========");
        printListOfUsers(usersList1);
        ArrayList<User> usersList2 = new ArrayList<>();
        usersList2.add(user13);
        usersList2.add(user14);
        usersList2.add(user100);
        System.out.println("=========List 2=========");
        printListOfUsers(usersList2);
        System.out.println("=========Equals=========");
        printListOfUsers(Utils.findEquals(usersList1, usersList2));

        System.out.println("=========Find equals from sets==================");
        HashSet<User> usersSet1 = new HashSet<>();
        usersSet1.add(user1);
        usersSet1.add(user3);
        usersSet1.add(user4);
        usersSet1.add(user5);
        System.out.println("=========Set 1=========");
        printSetOfUsers(usersSet1);
        HashSet<User> usersSet2 = new HashSet<>();
        usersSet2.add(user13);
        usersSet2.add(user14);
        usersSet2.add(user100);
        System.out.println("=========Set 2=========");
        printSetOfUsers(usersSet2);
        System.out.println("=========Equals=========");
        printSetOfUsers(Utils.findEquals(usersSet1, usersSet2));
    }

    static private void printListOfUsers(List<User> usersList) {
        for (int i = 0; i < usersList.size(); i++) {
            System.out.println(i + " : " + usersList.get(i).getName() + " is " + usersList.get(i).getAge() + " years old");
        }
    }

    static private void printSetOfUsers(Set<User> usersSet) {
        for (User currentUser : usersSet) {
            System.out.println(currentUser.getName() + " is " + currentUser.getAge() + " years");
        }
    }
}
