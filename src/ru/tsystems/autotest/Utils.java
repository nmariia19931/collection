package ru.tsystems.autotest;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Utils {

    public static <T> List<T> findEquals(List<T> list1, List<T> list2) {
        List<T> result = new ArrayList<T>();
        for (T entity : list2) {
            if (list1.contains(entity)) {
                result.add(entity);
            }
        }
        return result;
    }

    public static <T> Set<T> findEquals(Set<T> set1, Set<T> set2) {
        Set<T> result = new HashSet<T>();
        for (T entity : set2) {
            if (set1.contains(entity)) {
                result.add(entity);
            }
        }
        return result;
    }

}
