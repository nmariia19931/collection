package ru.tsystems.autotest;

public class User {
    private int age;
    private String name;

    public User(int age, String name) {
        this.age = age;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof User) {
            User objUser = (User) obj;
            if ((this.getAge() == objUser.getAge()) && (this.getName().equals(objUser.getName()))) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        return this.getAge() + this.getName().hashCode();
    }
}
